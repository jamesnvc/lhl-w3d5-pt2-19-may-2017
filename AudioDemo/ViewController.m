//
//  ViewController.m
//  AudioDemo
//
//  Created by James Cash on 19-05-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
@import AVFoundation;

@interface ViewController ()

@property (nonatomic,strong) NSURL* savedAudioURL;

@property (nonatomic,strong) AVAudioRecorder *recorder;

@property (nonatomic,strong) AVAudioPlayer *player;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Get the app's document directory
    NSString *docsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];

    // make the URL we will save/play from be a file called recording.m4a in that directory
    self.savedAudioURL = [NSURL fileURLWithPath:[docsDir stringByAppendingPathComponent:@"recording.m4a"]];
}

- (IBAction)toggleRecord:(UIButton *)sender {
    if (self.recorder.recording) {
        [self.recorder stop];
        [sender setTitle:@"Record" forState:UIControlStateNormal];
    } else {
        [sender setTitle:@"Stop" forState:UIControlStateNormal];

        NSError *err = nil;

        self.recorder = [[AVAudioRecorder alloc]
                         initWithURL:self.savedAudioURL
                         settings:@{AVFormatIDKey: @(kAudioFormatMPEG4AAC),
                                    AVSampleRateKey: @(44100),
                                    AVNumberOfChannelsKey: @(2)}
                         error:&err];

        if (err != nil) {
            NSLog(@"Error creating recorder: %@", err.localizedDescription);
        }

        [self.recorder record];
    }
}

- (IBAction)togglePlay:(UIButton *)sender {
    if (self.player.playing) {
        [self.player stop];
        [sender setTitle:@"Play" forState:UIControlStateNormal];
    } else {
        [sender setTitle:@"Stop" forState:UIControlStateNormal];

        NSError *err = nil;
        self.player = [[AVAudioPlayer alloc] initWithContentsOfURL:self.savedAudioURL error:&err];

        if (err != nil) {
            NSLog(@"Error creating player %@", err);
        }

        [self.player play];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
